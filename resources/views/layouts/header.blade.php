<!DOCTYPE html>
<html>
  <head>
    <title>Vigacom Beacons</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700" type="text/css">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('public/assets/icons/fav.ico') }}" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/stylesheets/font-awesome.min.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link href="{{ asset('public/css/all.css') }}" media="all" rel="stylesheet" type="text/css" />
    <!-- custom css -->
    <link href="{{ asset('public/css/app.css') }}" media="all" rel="stylesheet" type="text/css" />
  </head>
  <body class="page-header-fixed bg-3">
    <div class="modal-shiftfix main-wrapper">
      <!-- Navigation -->
      <div class="navbar navbar-fixed-top scroll-hide">
        <div class="container-fluid top-bar">
          <div class="pull-right">
            <ul class="nav navbar-nav pull-right">
              <li class="dropdown user hidden-xs">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                  <img width="34" height="34" src="{{ asset('public/assets/images/default-avatar.jpg') }}" />
                  Test Admin<b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="#"><i class="fa fa-user"></i>My Account</a>
                  </li>
                  <li>
                    <a href="{{ url('/logout') }}"><i class="fa fa-sign-out"></i>Logout</a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
          <button class="navbar-toggle">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <img class="header-main-icon" src="{{ asset('public/assets/icons/vigacom-logo.png') }}">
        </div>
        <div class="container-fluid main-nav clearfix">
          <div class="nav-collapse">
            <ul class="nav">
              <li>
                <a class="@if(Request::segment(1) == 'beacons'){{ "current" }}@endif" href="{{ url('beacons') }}"><span class="ion-disc"></span>Beacons</a>
              </li>
              <li>
                <a class="@if(Request::segment(1) == 'analytics'){{ "current" }}@endif" href="{{ url('analytics') }}"><span class="ion-ios-pulse-strong"></span>Analytics</a>
              </li>
              <li>
                <a href="#"><span class="ion-ios-cog"></span>Settings</a>
              </li>
              <li>
                <a href="#"><span class="ion-pull-request"></span>Here & Now</a>
              </li>
              <li class="dropdown">
                <a data-toggle="dropdown" href="#" class="@if(Request::segment(1) == 'users'){{ "current" }}@endif">
                  <span class="ion-android-people"></span>Users <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <a class="@if(Request::segment(1) == 'users' && Request::segment(2) == 'new'){{'text-red'}}@endif" href="{{ url('/users/new') }}">Add User</a>
                    <a class="@if(Request::segment(1) == 'users' && Request::segment(2) == ''){{'text-red'}}@endif" href="{{ url('/users') }}">List/Edit Users</a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div><!-- End Navigation -->