<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shortened_Url extends Model
{
  /**
   * [$table description]
   * @var string
   */
  protected $table = 'shortened_urls';
  /**
   * [$timestamps description]
   * @var boolean
   */
  public $timestamps = false;
  /**
   * [$fillable description]
   * @var [type]
   */
  protected $fillable = ['long_url', 'code', 'creator', 'referrals', 'created_at', 'updated_at'];

  /**
   * [stat_clicks description]
   * @return [type] [description]
   */
  public function stat_clicks() {
    return $this->hasMany('App\Models\Stat_Click');
  }
  /**
   * [stat_impressions description]
   * @return [type] [description]
   */
  public function stat_impressions() {
    return $this->hasMany('App\Models\Stat_Impression');
  }
  /**
   * [attachment_url description]
   * @return [type] [description]
   */
  public function attachment_url() {
    return $this->belongsTo('App\Models\Attachment_Url');
  }
}
