    </div><!-- .modal-shiftfix.main-wrapper end here -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript">
      $.ajaxSetup({
        beforeSend: function(xhr, type) {
          if (!type.crossDomain) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
          }
        }
      });
    </script>
    <script type="text/javascript" src="{{ asset('public/js/all.js') }}"></script>
    <!-- custom js -->
    <script src="{{ asset('public/js/app.js') }}" type="text/javascript"></script>
  </body>
</html>