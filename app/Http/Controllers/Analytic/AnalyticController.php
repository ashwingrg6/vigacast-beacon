<?php

namespace App\Http\Controllers\Analytic;

use Illuminate\Http\Request;
use App\Http\Controllers\MyController;

class AnalyticController extends MyController {
  public $google_token_data;

  public function __construct(){
    parent::__construct();
    $this->google_token_data = json_decode(file_get_contents(storage_path()."/json/google_token.json"), true);
  }
  /**
   * [getIndex description]
   * @return [type] [description]
   */
  public function getIndex() {
    // $this->googleClient();
    $file_path = 'http://localhost/url/stat.json';
    if (\App::environment() == 'production') {
      $file_path = 'https://vc1.io/stat.json';
    }
    $result = file_get_contents($file_path);
    $data['analytics_data'] = json_decode($result, true);
    // $data['beacon_attachments'] = $this->getAttachments();
    return view('analytics/index_view')->with($data);
  }

  public function getAttachments() {
    $beacon_name = "beacons/3!45324335364442354446443046354137";
    $client = new \GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer '.$this->google_token_data['access_token']], 'Accept' => 'application/json']);
    $res = $client->get("https://proximitybeacon.googleapis.com/v1beta1/$beacon_name/attachments?namespacedType=*/*");
    if ($res->getStatusCode() == 200) {
      $attachments = json_decode($res->getBody(), TRUE);
      return $attachments;
    }
    echo "error";die;
  }

}
