<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
  use Notifiable;
  /**
   * [$table description]
   * @var string
   */
  protected $table = 'users';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['name', 'email', 'type', 'password'];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token',
  ];

  /**
   * [beacons description]
   * @return [type] [description]
   */
  public function beacons() {
    return $this->hasMany('App\Models\Beacon');
  }

  /**
   * [allUsers description]
   * @return [type] [description]
   */
  public function allUsers(){
    return $this->get();
  }
  /**
   * [insertUser description]
   * @param  [type] $data [description]
   * @return [type]       [description]
   */
  public function insertUser($data){
    $data['password'] = bcrypt($data['password']);
    return $this->insertGetId($data);
  }
  /**
   * [deleteUser description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function deleteUser($id){
    return $this->where('id', $id)->delete();
  }

}
