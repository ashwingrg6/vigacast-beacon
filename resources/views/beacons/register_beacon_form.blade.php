<div class="modal-header">
  <button aria-hidden="true" class="close" data-dismiss="modal" type="button">×</button>
  <h4 class="modal-title"><i class="ion-plus-circled ion-md-size">&nbsp;</i>Register New Beacon</h4>
</div>
<div class="modal-body" id="add-beacon-modal-body">
  <!--<div class="alert alert-danger form-message">
    <button class="close" data-dismiss="alert" type="button">×</button><i class="ion-alert-circled ion-md-size">&nbsp;</i>Error! There are some errors, please correct them below.
  </div>
  <div class="alert alert-success form-message">
    <button class="close" data-dismiss="alert" type="button">×</button><i class="ion-checkmark-circled ion-md-size">&nbsp;</i>Success! New beacon has been successfully registered.
  </div>-->
  <!-- add beacon form starts here -->
  <form action="" id="register-beacon-form" method="post" novalidate="novalidate">
    <fieldset>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="">Beacon Name *</label>
            <input class="form-control" name="name" type="text" placeholder="Beacon Name">
            {{-- <label class="form-error">The beacon name field is required.</label> --}}
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="">Status *</label>
            <select class="form-control" name="status">
              <option value="0">Active</option>
              <option value="1">Inactive</option>
            </select>
          </div>
        </div>

      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="">UUID *</label>
            <input class="form-control" name="uuid" type="text" placeholder="Enter UUID">
            {{-- <label class="form-error">The UUID field is required.</label> --}}
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="">Expected Stability *</label>
            <select class="form-control" name="stability">
              <option value="stable">Stable</option>
              <option value="portable">Portable</option>
              <option value="mobile">Mobile</option>
              <option value="roving">Roving</option>
            </select>
          </div>
        </div>

      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="">Namespace ID</label>
            <input class="form-control" name="namespace_id" type="text" placeholder="Namespace ID">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="">Instanace ID</label>
            <input class="form-control" name="instance_id" type="text" placeholder="Instance ID">
          </div>
        </div>

      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="">Note</label>
            <textarea class="form-control" name="note" type="text" placeholder="Enter Note...."></textarea>
          </div>
        </div>

      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="">Attachment URL Title</label>
            <input class="form-control" name="attachment_url_title" type="text" placeholder="URL Title">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="">Attachment URL</label>
            <input class="form-control" name="attachment_url" type="text" placeholder="Attachment URL">
          </div>
        </div>
      </div>
    </fieldset>
  </form><!-- add beacon form ends -->
</div>
<div class="modal-footer">
  <button class="btn red-primary form-btn register-beacon-submit" type="button"><i class="ion-checkmark-circled ion-md-size">&nbsp;</i><span>Save</span></button>
  <button class="btn gray-primary form-btn" data-dismiss="modal" type="button"><i class="ion-close-circled ion-md-size">&nbsp;</i><span>Cancel</span></button>
</div>