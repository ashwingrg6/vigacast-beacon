<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Google_Client;
use App;
use App\Models\User as User;
use App\Models\Beacon as Beacon;
use App\Models\Shortened_Url as Shortened_Url;
use App\Models\Attachment_Url as Attachment_Url;
use App\Models\Stat_Click as Stat_Click;
use App\Models\Stat_Impression as Stat_Impression;
use Validator;
use Carbon\Carbon;

class MyController extends Controller {

  /**
   * [__construct description]
   */
  public function __construct() {
    $this->user_model = new User;
    $this->beacon_model = new Beacon;
    $this->stat_click_model = new Stat_Click;
    $this->stat_impression_model = new Stat_Impression;
  }

  /**
   * [googleClient description]
   * @return [type] [description]
   */
  public function googleClient() {
    $path = storage_path() . "/json/client_secret.json";
    $url_origin = 'http://localhost/vigacast-beacon/';
    if (App::environment() == 'production') {
      $path = storage_path() . "/json/live_vigacast_client_secret.json";
      $url_origin = 'https://vigacast.com/beacons-cloud/';
    }
    $clientData = json_decode(file_get_contents($path), true);
    $client = new Google_Client();
    // $client->setApplicationName($this->projectName);
    $client->setScopes('https://www.googleapis.com/auth/userlocation.beacon.registry');
    $client->setAuthConfig($clientData);
    $client->setRedirectUri($url_origin.'callback');
    $client->setAccessType('offline');
    $client->setApprovalPrompt('force');
    // Load previously authorized credentials from a file.
    $google_token_data = json_decode(file_get_contents(storage_path()."/json/google_token.json"), true);
    if (!empty($google_token_data)) {
      $accessToken = $google_token_data;
    }
    else {
      // Request authorization from the user.
      $authUrl = $client->createAuthUrl();
      header('Location: ' . filter_var($authUrl, FILTER_SANITIZE_URL));
      if (isset($_GET['code'])) {
        $authCode = $_GET['code'];
        // Exchange authorization code for an access token.
        $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
        header('Location: ' . filter_var($this->redirectUri, FILTER_SANITIZE_URL));
        file_put_contents(storage_path()."/json/google_token.json", json_encode($accessToken));
      }
      else{
        exit('No code found');
      }
    }
    $client->setAccessToken($accessToken);
    // Refresh the token if it's expired.
    if ($client->isAccessTokenExpired()) {
      // save refresh token to some variable
      $refreshTokenSaved = $client->getRefreshToken();
      // update access token
      $client->fetchAccessTokenWithRefreshToken($refreshTokenSaved);
      // pass access token to some variable
      $accessTokenUpdated = $client->getAccessToken();
      // append refresh token
      $accessTokenUpdated['refresh_token'] = $refreshTokenSaved;
      //Set the new acces token
      $accessToken = $refreshTokenSaved;
      $client->setAccessToken($accessToken);
      // save to file
      // file_put_contents($this->tokenFile, json_encode($accessTokenUpdated));
      file_put_contents(storage_path()."/json/google_token.json", json_encode($accessTokenUpdated));
    }
    return $client;
  }

  /**
   * [updateStat description]
   * @param  Request $request [description]
   * @return [type]           [description]
   */
  public function updateStat(Request $request) {
    $stat_type = $request->get('type');
    $data['short_url_id'] = $request->get('short_url_id');
    $data = $this->prepareInsertTimestamp($data);
    if ($stat_type == 'click') {
      $insert_id = $this->stat_click_model->insertStatClick($data);
      return $insert_id;
    }
    elseif($stat_type == 'impression') {
      $insert_id = $this->stat_impression_model->insertStatImpression($data);
      return $insert_id;
    }
    else {
      echo "Invalid arguments";die;
    }
  }

  /**
   * [prepareInsertTimestamp description]
   * @param  [type] $data [description]
   * @return [type]       [description]
   */
  public function prepareInsertTimestamp($data) {
    $data['created_at'] = $data['updated_at'] = Carbon::now()->format('Y-m-d H:i:s');
    return $data;
  }

  /**
   * [constructData description]
   * @param  [type] $request [description]
   * @return [type]          [description]
   */
  public function constructData($request) {
    $post_data = array();
    foreach ($request::input() as $key => $value) {
      $post_data[$key] = $value;
    }
    return $post_data;
  }

  /**
   * [constructRequestData description]
   * @param  [type] $request [description]
   * @return [type]          [description]
   */
  public function constructRequestData($request) {
    $post_data = array();
    foreach ($request->input() as $key => $value) {
      $post_data[$key] = $value;
    }
    unset($post_data['_token'], $post_data['password_confirmation']);
    return $post_data;
  }

}
