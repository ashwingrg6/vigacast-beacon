<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stat_Impression extends Model
{
  /**
   * [$table description]
   * @var string
   */
  protected $table = 'stat_impressions';
  /**
   * [$fillable description]
   * @var [type]
   */
  protected $fillable = ['short_url_id'];

  /**
   * [Short_Url description]
   */
  public function Short_Url(){
    return $this->belongsTo('App\Models\Shortened_Url', 'short_url_id');
  }

  /**
   * [allStatImpressions description]
   * @return [type] [description]
   */
  public function allStatImpressions(){
    return $this->get();
  }

  /**
   * [insertStatImpression description]
   * @param  [type] $data [description]
   * @return [type]       [description]
   */
  public function insertStatImpression($data){
    return $this->insertGetId($data);
  }

  /**
   * [getStatImpression description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function getStatImpression($id){
    return $this->where('id', $id)->get();
  }

  /**
   * [updateStatImpression description]
   * @param  [type] $id   [description]
   * @param  [type] $data [description]
   * @return [type]       [description]
   */
  public function updateStatImpression($id, $data){
    return $this->where('id', $id)->update($data);
  }

  /**
   * [deleteStatImpression description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function deleteStatImpression($id){
    return $this->where('id', $id)->delete();
  }

}
