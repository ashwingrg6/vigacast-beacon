let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/js/app.js', 'public/js')
   // .sass('resources/assets/sass/app.scss', 'public/css');

mix.styles([
  'public/assets/stylesheets/bootstrap.min.css',
  // 'public/assets/stylesheets/font-awesome.min.css',
  'public/assets/stylesheets/hightop-font.css',
  'public/assets/stylesheets/isotope.css',
  'public/assets/stylesheets/jquery.fancybox.css',
  'public/assets/stylesheets/fullcalendar.css',
  'public/assets/stylesheets/wizard.css',
  'public/assets/stylesheets/select2.css',
  'public/assets/stylesheets/morris.css',
  'public/assets/stylesheets/datatables.css',
  'public/assets/stylesheets/datepicker.css',
  'public/assets/stylesheets/timepicker.css',
  'public/assets/stylesheets/colorpicker.css',
  'public/assets/stylesheets/bootstrap-switch.css',
  'public/assets/stylesheets/bootstrap-editable.css',
  'public/assets/stylesheets/daterange-picker.css',
  'public/assets/stylesheets/typeahead.css',
  'public/assets/stylesheets/summernote.css',
  'public/assets/stylesheets/ladda-themeless.min.css',
  'public/assets/stylesheets/social-buttons.css',
  'public/assets/stylesheets/jquery.fileupload-ui.css',
  'public/assets/stylesheets/dropzone.css',
  'public/assets/stylesheets/nestable.css',
  'public/assets/stylesheets/pygments.css',
  'public/assets/stylesheets/style.css'
], 'public/css/all.css');

mix.combine([
  'public/assets/javascripts/bootstrap.min.js',
  'public/assets/javascripts/raphael.min.js',
  'public/assets/javascripts/selectivizr-min.js',
  'public/assets/javascripts/jquery.mousewheel.js',
  'public/assets/javascripts/jquery.vmap.min.js',
  'public/assets/javascripts/jquery.vmap.sampledata.js',
  'public/assets/javascripts/jquery.vmap.world.js',
  'public/assets/javascripts/jquery.bootstrap.wizard.js',
  'public/assets/javascripts/fullcalendar.min.js',
  'public/assets/javascripts/gcal.js',
  'public/assets/javascripts/jquery.dataTables.min.js',
  'public/assets/javascripts/datatable-editable.js',
  'public/assets/javascripts/jquery.easy-pie-chart.js',
  'public/assets/javascripts/excanvas.min.js',
  'public/assets/javascripts/jquery.isotope.min.js',
  'public/assets/javascripts/isotope_extras.js',
  'public/assets/javascripts/modernizr.custom.js',
  'public/assets/javascripts/jquery.fancybox.pack.js',
  // 'public/assets/javascripts/application.js',
  'public/assets/javascripts/select2.js',
  'public/assets/javascripts/styleswitcher.js',
  'public/assets/javascripts/wysiwyg.js',
  'public/assets/javascripts/typeahead.js',
  'public/assets/javascripts/summernote.min.js',
  'public/assets/javascripts/jquery.inputmask.min.js',
  'public/assets/javascripts/jquery.validate.js',
  'public/assets/javascripts/bootstrap-fileupload.js',
  'public/assets/javascripts/bootstrap-datepicker.js',
  'public/assets/javascripts/bootstrap-timepicker.js',
  'public/assets/javascripts/bootstrap-colorpicker.js',
  'public/assets/javascripts/bootstrap-switch.min.js',
  'public/assets/javascripts/typeahead.js',
  'public/assets/javascripts/spin.min.js',
  'public/assets/javascripts/ladda.min.js',
  'public/assets/javascripts/moment.js',
  'public/assets/javascripts/mockjax.js',
  'public/assets/javascripts/bootstrap-editable.min.js',
  'public/assets/javascripts/xeditable-demo-mock.js',
  'public/assets/javascripts/xeditable-demo.js',
  'public/assets/javascripts/address.js',
  'public/assets/javascripts/daterange-picker.js',
  'public/assets/javascripts/date.js',
  'public/assets/javascripts/morris.min.js',
  'public/assets/javascripts/skycons.js',
  'public/assets/javascripts/fitvids.js',
  'public/assets/javascripts/jquery.sparkline.min.js',
  'public/assets/javascripts/dropzone.js',
  'public/assets/javascripts/jquery.nestable.js',
  'public/assets/javascripts/main.js',
  'public/assets/javascripts/respond.js'
], 'public/js/all.js')
mix.js('resources/assets/js/app.js', 'public/js');
mix.styles(['resources/assets/css/app.css', 'resources/assets/css/responsive.css'], 'public/css/app.css');
