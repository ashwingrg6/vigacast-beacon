<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatClicksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stat_clicks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('short_url_id')->unsigned();
            $table->timestamps();
            $table->foreign('short_url_id')
                    ->references('id')
                    ->on('shortened_urls')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stat_clicks');
    }
}
