<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $table_data = array(
      'name' => 'Main Admin',
      'email' => 'admin@admin.com',
      'type' => 'admin',
      'password' => bcrypt('password')
    );
    DB::table('users')->insert($table_data);
  }
}
