<?php

namespace App\Http\Controllers\Auth;

// use App\Http\Controllers\Controller;
use App\Http\Controllers\MyController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\PostUserLogin;

class LoginController extends MyController
{
  /*
  |--------------------------------------------------------------------------
  | Login Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles authenticating users for the application and
  | redirecting them to your home screen. The controller uses a trait
  | to conveniently provide its functionality to your applications.
  |
  */

  use AuthenticatesUsers;

  /**
   * Redirect location /login, if user is not successfully authenticated.
   * @var string $loginPath
   */
  protected $loginPath = '/login';

  /**
   * Redirect location /dashboard, if user is successfully authenticated.
   * @var string $redirectPath
   */
  protected $redirectPath = '/beacons';

  /**
   * Redirect location after use is logged out.
   * @var string $redirectAfterLogout
   */
  protected $redirectAfterLogout = '/login';

  /**
   * Define maximum number of login attempts.
   * @var integer $maxLoginAttempts
   */
  protected $maxLoginAttempts = 10;

  /**
   * Create a new authentication controller instance.
   * @return void
   */
  public function __construct(){
    $this->middleware('guest', ['except' => 'getLogout']);
  }

  /**
   * [getLogin description]
   * @param  \Request $request [description]
   * @return [type]            [description]
   */
  public function getLogin(\Request $request) {
    return view('auth.login');
  }

  /**
   * [postLogin description]
   * @return [type] [description]
   */
  public function postLogin(PostUserLogin $request) {
    $data = $this->constructRequestData($request);
    if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
      return redirect('beacons');
    }
    return redirect('login')->with('login_error', 'Invalid Credentials.')->withInput();
  }

  /**
   * [getLogout description]
   * @return [type] [description]
   */
  public function getLogout(\Request $request) {
    Auth::logout();
    return redirect('login')->with('logout_msg', 'You are now signed out!');
  }

}
