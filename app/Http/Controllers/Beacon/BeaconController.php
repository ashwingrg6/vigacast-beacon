<?php

namespace App\Http\Controllers\Beacon;

use App\Http\Controllers\MyController;

class BeaconController extends MyController {
  public $google_token_data;

  public function __construct(){
    parent::__construct();
    $this->google_token_data = json_decode(file_get_contents(storage_path()."/json/google_token.json"), true);
  }

  /**
   * [getIndex description]
   * @return [type] [description]
   */
  public function getIndex(\Request $request) {
    // $this->googleClient();
    // $data['google_beacons'] = $this->getAll();
    return view('beacons/index_view');
  }

  /**
   * [getAll description]
   * @return [type] [description]
   */
  public function getAll() {
    $client = new \GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer '.$this->google_token_data['access_token']], 'Accept' => 'application/json']);
    $res = $client->get('https://proximitybeacon.googleapis.com/v1beta1/beacons');
    if ($res->getStatusCode() == 200) {
      $all_beacons = json_decode($res->getBody(), TRUE);
      return $all_beacons;
    }
    echo "error";die;
  }

  /**
   * [postRegisterBeacon description]
   * @return [type] [description]
   */
  public function postRegisterBeacon(\Request $request) {
    //validate user inputs
    $validator = \Validator::make($request::all(), $this->beacon_model->register_beacon_validation_rules);
    if ($validator->fails()) {
      return view('beacons.register_beacon_form')
        ->withErrors($validator);
        // ->withInput();
    }//End validator fails if
    echo "passed";die;
    $data = $this->constructData($request);
  }

}
