<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return redirect('/login');
});
//Authentication routes
Route::get('login',['as' => 'login', 'uses' => 'Auth\LoginController@getLogin']); //get login
Route::post('login', 'Auth\LoginController@postLogin'); //post login
Route::get('logout', 'Auth\LoginController@getLogout'); //get logout

Route::group(['middleware' => ['auth']], function () {
  AdvancedRoute::controllers([
    '/beacons' => 'Beacon\BeaconController',
    '/analytics' => 'Analytic\AnalyticController',
    '/users' => 'User\UserController'
  ]);
});

Route::get('/clear-cache', function() {
  $exitCode = Artisan::call('cache:clear');
  // return what you want
});
Route::get('/clear-session', function() {
  session()->forget('access_token');
});
Route::get('/url-command', function() {
  // Artisan::call('schedule:run');
  Artisan::call('beacon_urls:change');
});

Route::get('/callback', function () {
  $path = storage_path() . "/json/client_secret.json";
  $url_origin = 'http://localhost/vigacast-beacon/';
  if (App::environment() == 'production') {
    $path = storage_path() . "/json/live_vigacast_client_secret.json";
    $url_origin = 'https://vigacast.com/beacons-cloud/';
  }
  $clientData = json_decode(file_get_contents($path), true);
  $client = new Google_Client();
  $client->setAuthConfigFile($clientData);
  // $client->setApprovalPrompt('force');
  $client->setPrompt('consent');
  $client->setRedirectUri($url_origin.'callback');
  $client->addScope('https://www.googleapis.com/auth/userlocation.beacon.registry');
  $client->setAccessType('offline');        // offline access
  $client->setIncludeGrantedScopes(true);

  if (!isset($_GET['code'])) {
    $auth_url = $client->createAuthUrl();
    header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
  } else {
    $client->authenticate($_GET['code']);
    $token_array = $client->getAccessToken();
    file_put_contents(storage_path()."/json/google_token.json", json_encode($token_array));
    return redirect('beacons');
  }
});
/**
 *
 */
Route::get('/update-stat', 'MyController@updateStat');
