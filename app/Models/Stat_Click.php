<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stat_Click extends Model
{
  /**
   * [$table description]
   * @var string
   */
  protected $table = 'stat_clicks';
  /**
   * [$fillable description]
   * @var [type]
   */
  protected $fillable = ['short_url_id'];

  /**
   * [Short_Url description]
   */
  public function Short_Url(){
    return $this->belongsTo('App\Models\Shortened_Url', 'short_url_id');
  }

  /**
   * [allStatClicks description]
   * @return [type] [description]
   */
  public function allStatClicks(){
    return $this->get();
  }

  /**
   * [insertStatClick description]
   * @param  [type] $data [description]
   * @return [type]       [description]
   */
  public function insertStatClick($data){
    return $this->insertGetId($data);
  }

  /**
   * [getStatClick description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function getStatClick($id){
    return $this->where('id', $id)->get();
  }

  /**
   * [updateStatClick description]
   * @param  [type] $id   [description]
   * @param  [type] $data [description]
   * @return [type]       [description]
   */
  public function updateStatClick($id, $data){
    return $this->where('id', $id)->update($data);
  }

  /**
   * [deleteStatClick description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function deleteStatClick($id){
    return $this->where('id', $id)->delete();
  }

}
