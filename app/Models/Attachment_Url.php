<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attachment_Url extends Model
{
  /**
   * [$table description]
   * @var string
   */
  protected $table = 'attachment_urls';
  /**
   * [$fillable description]
   * @var [type]
   */
  protected $fillable = ['beacon_id', 'short_url_id', 'attachment_url_title', 'attachment_url'];
  /**
   * [beacon description]
   * @return [type] [description]
   */
  public function beacon() {
    return $this->belongsTo('App\Models\Beacon');
  }
  /**
   * [shortened_url description]
   * @return [type] [description]
   */
  public function shortened_url() {
    return $this->hasOne('App\Models\Shortened_Url', 'short_url_id', 'id');
  }
}
