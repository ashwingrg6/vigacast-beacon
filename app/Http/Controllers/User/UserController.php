<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Http\Requests;
use App\Http\Controllers\MyController;
use App\Http\Requests\PostNewUser;

class UserController extends MyController {
  /**
   * [__construct description]
   */
  public function __construct(){
    parent::__construct();
  }

  /**
   * [getIndex description]
   * @param  Request $request [description]
   * @return [type]           [description]
   */
  public function getIndex(Request $request) {
    $data['users'] = $this->user_model->allUsers();
    return view("users.all_users")->with($data);
  }

  /**
   * [getNew description]
   * @return [type] [description]
   */
  public function getNew() {
    return view('users.new_user');
  }

  /**
   * [postNew description]
   * @param  Request $request [description]
   * @return [type]           [description]
   */
  public function postNew(PostNewUser $request) {
    $data = $this->constructRequestData($request);
    try {
      $user_id = $this->user_model->insertUser($this->prepareInsertTimestamp($data));
      return redirect('users/new')->with('success_msg', 'Success! New user has been successfully registered.');
    } catch (\Exception $e) {
      return redirect('users/new')->with('app_error', 'Error! '.$e->getMessage());
    }
  }

  /**
   * [getDelete description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function getDelete($id) {
    try {
      $delete_flag = $this->user_model->deleteUser($id);
      return redirect('users')->with('success_msg', 'Success! Selected user has been deleted from database.');
    } catch (\Exception $e) {
      return redirect('users')->with('app_error', 'Error! '.$e->getMessage());
    }
  }

}
