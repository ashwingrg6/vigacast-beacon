@include('layouts.header')
<div class="container-fluid main-content">
  @yield('main-content')
</div>
@include('layouts.footer')