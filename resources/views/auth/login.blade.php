<!DOCTYPE html>
<html>
  <head>
    <title>Vigacom Beacons</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700" type="text/css">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('public/assets/icons/fav.ico') }}" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/stylesheets/font-awesome.min.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link href="{{ asset('public/css/all.css') }}" media="all" rel="stylesheet" type="text/css" />
    <!-- custom css -->
    <link href="{{ asset('public/css/app.css') }}" media="all" rel="stylesheet" type="text/css" />
  </head>
  <body class="bg-3">
    <!-- main container startts -->
    <div class="container">
      <div class="row login-container">
        <div class="col-md-5 col-md-offset-1 login-left-section">
          <h1 class="login-title">Vigacom Beacons Mgmt.</h1>
          <img class="login-logo" src="{{ asset('public/assets/icons/vigacom-logo.png') }}">
        </div><!-- .login-left-section ends -->
        <!-- login-form wrapper -->
        <div class="col-md-5 login-form-wrapper">
          <h1 class="login-title">Login</h1>
          <form id="login-form" class="login-form" action="login" method="post">
            {{ csrf_field() }}
            @if (Session::has('logout_msg'))
              <label class="general-msg">{{ Session::get('logout_msg') }}</label>
            @endif
            @if (Session::has('login_error'))
              <label class="form-error main-error">{{ Session::get('login_error') }}</label>
            @endif
            <!-- email input -->
            <div class="row">
              <div class="form-group col-md-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                  <input class="form-control" placeholder="Email" type="text" name="email">
                </div>
                <label class="form-error">{{ $errors->first('email') }}</label>
              </div><!-- .form-group -->
            </div><!-- email input ends -->
            <!-- password input -->
            <div class="row">
              <div class="form-group col-md-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-lock" id="icon-psw"></i></span>
                  <input class="form-control" placeholder="Password" type="password" name="password">
                </div><!-- .input-group -->
                <label class="form-error">{{ $errors->first('password') }}</label>
              </div><!-- .form-group -->
            </div><!-- password input ends -->
            <!--form actions-->
            <div class="row">
              <div class="col-md-10" style="padding-right:10px;">
                <button class="btn btn-primary btn-cons pull-right btn2" type="submit" id="login-btn">Login</button>
              </div>
            </div><!--form actions-->
          </form>
        </div><!-- login-form wrapper ends -->
      </div>
    </div><!-- main container ends here -->
  </body>
</html>