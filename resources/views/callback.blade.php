@extends('layouts.master')
@section('main-content')

<div class="box box-default">
  @php
      {{
        // $clientJson = Storage::get('client_secret.json');
        // $clientData = json_decode($clientJson, true);
        $path = storage_path() . "/json/client_secret.json"; // ie: /var/www/laravel/app/storage/json/filename.json
        $clientData = json_decode(file_get_contents($path), true);
        // print_r($clientJson);die;
        $client = new Google_Client();
        $client->setAuthConfigFile($clientData);
        $client->setApprovalPrompt('force');
        $client->setRedirectUri('http://localhost/vigacast-beacon/callback');
        $client->addScope('https://www.googleapis.com/auth/userlocation.beacon.registry');
        $client->setAccessType('offline');        // offline access
        $client->setIncludeGrantedScopes(true);

        if (!isset($_GET['code'])) {
          $auth_url = $client->createAuthUrl();
          echo $auth_url;die;
          header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
        } else {
          $client->authenticate($_GET['code']);
          $token_array = $client->getAccessToken();
          // $_SESSION['access_token'] = $token_array['access_token'];
          session()->put('access_token', $token_array);
          // $redirect_uri = 'http://localhost/vigacast-beacon/beacons';
          // header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
        }
      }}
  @endphp
</div>

@stop