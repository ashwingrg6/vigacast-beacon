<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Beacon extends Model
{
  /**
   * [$table description]
   * @var string
   */
  protected $table = 'beacons';
  /**
   * [$fillable description]
   * @var [type]
   */
  protected $fillable = ['user_id', 'name', 'stability', 'status', 'uuid', 'namespace_id', 'instance_id', 'advertise_id', 'note'];
  /**
   * [$register_beacon_validation_rules description]
   * @var array
   */
  public $register_beacon_validation_rules = [
    'name' => 'required',
    'stability' => 'required',
    'status' => 'required',
    'uuid' => 'required|unique:beacons,uuid',
    'advertise_id' => 'required'
  ];
  /**[post description]
   *
   * @return [type] [description]
   */
  public function user() {
    return $this->belongsTo('App\Models\User', 'user_id');
  }
  /**
   * [attachment_urls description]
   * @return [type] [description]
   */
  public function attachment_urls() {
    return $this->hasMany('App\Models\Attachment_Url');
  }
}
