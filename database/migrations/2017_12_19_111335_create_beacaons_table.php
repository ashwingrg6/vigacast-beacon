<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeacaonsTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('beacons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name');
            $table->enum('stability', ['stable', 'portable', 'mobile', 'roving']);
            $table->unsignedTinyInteger('status');
            $table->string('uuid');
            $table->string('namespace_id')->nullable();
            $table->string('instance_id')->nullable();
            $table->string('advertise_id');
            $table->text('note')->nullable();
            $table->timestamps();
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onUpdate('cascade');
          ;
        });
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::dropIfExists('beacons');
    }
}
