@extends('layouts.master')
@section('main-content')
<div class="page-title">
  <li>YOU ARE HERE</li>
  <li><a href="javascript:void(0);">Analytics</a></li>
</div>
<div class="analytics-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <!-- data/section heading -->
      <div class="widget-container fluid-height clearfix">
        <!-- content heading-section -->
        <div class="heading table-heading analytic-section-heading">
          <div class="analytic-filter-wrapper">
            <form action="#" class="" id="analytic-filter-form">
              <div class="form-group col-md-3">
                <select class="form-control md-input">
                  <option value="">Test Beacon 1</option>
                  <option value="">Test Beacon 2</option>
                  <option value="">Test Beacon 3</option>
                  <option value="">Test Beacon 4</option>
                </select>
              </div>
              <div class="form-group col-md-2">
                <select class="form-control md-input analytic-type-select">
                  <option value="">Last 7 days</option>
                  <option value="">Last 15 days</option>
                  <option value="">Last Month</option>
                  <option value="date_range">Select Date Range</option>
                </select>
              </div>
              <div class="form-group col-md-2" id="input-date-from">
                <input class="form-control md-input" data-date-autoclose="true" data-date-format="dd-mm-yyyy" id="dpd1" placeholder="Date From" type="text">
              </div>
              <div class="form-group col-md-2" id="input-date-until">
                <input class="form-control md-input" data-date-autoclose="true" data-date-format="dd-mm-yyyy" id="dpd2" placeholder="Date Until" type="text">
              </div>
              <div class="form-group col-md-2">
                <button class="btn btn-primary form-red-primary-btn" type="submit"><i class="ion-android-refresh ion-md-size">&nbsp;&nbsp;</i>Refresh</button>
              </div>
              {{-- <div class="form-group"></div> --}}
            </form>
          </div>
        </div><!-- .table-heading section ends -->
        <!-- analytics body-section -->
        <div class="row analytic-body-wrapper">
          <!-- analytic-top-tiles -->
          <div class="col-md-12">
            <div class="stats-container">
              <div class="col-md-4">
                <div class="number medium-gray-text"><i class="ion-arrow-graph-up-right ion-lg-size">&nbsp;&nbsp;</i>45</div>
                <div class="text normal-tile-text text-dark-gray">Total Impressions</div>
              </div>
              <div class="col-md-4">
                <div class="number medium-gray-text"><i class="ion-share ion-lg-size">&nbsp;</i>26</div>
                <div class="text normal-tile-text text-dark-gray">Total Number of Clicks</div>
              </div>
              <div class="col-md-4">
                <div class="number medium-gray-text"><i class="ion-ios-color-filter-outline ion-lg-size">&nbsp;</i>92</div>
                <div class="text normal-tile-text text-dark-gray">Total Number of Days being active</div>
              </div>
            </div>
          </div><!-- analytic-top-tiles section ends here -->
        </div><!-- analytic body section ends here -->


      </div><!-- .widget-container ends -->

    </div><!-- .col-lg-12 ends -->
  </div><!-- .row -->
  <!-- charts section -->
  <div class="row">
    <!-- sample line chart -->
    <div class="col-md-6">
      <div class="widget-container">
        <div class="heading custom-section-heading">
          <i class="fa fa-bar-chart-o"></i>Test Line Chart
        </div>
        <div class="widget-content padded">
          <div class="chart-graph line-chart">
            <div id="linechart-2">
              <canvas width="576" height="226" style="display: inline-block; width: 576px; height: 226px; vertical-align: top;"></canvas>
            </div>
            <ul class="chart-text-axis">
              <li>1</li>
              <li>2</li>
              <li>3</li>
              <li>4</li>
              <li>5</li>
              <li>6</li>
              <li>7</li>
              <li>8</li>
              <li>9</li>
              <li>10</li>
              <li>11</li>
              <li>12</li>
            </ul>
            <!-- end Line Chart -->
          </div>
        </div>
      </div>
    </div><!-- sample line chart ends here -->
    <!-- sample line chart -->
    <div class="col-md-6">
      <div class="widget-container second-chart">
        <div class="heading custom-section-heading">
          <i class="fa fa-bar-chart-o"></i>Test Line Chart 2
        </div>
        <div class="widget-content padded">

        </div>
      </div>
    </div><!-- sample line chart ends here -->
  </div><!-- charts section ends here -->
  <div class="row">
    <!-- impressions table -->
    <div class="col-md-6">
      <div class="widget-container fluid-height clearfix">
        <div class="heading custom-section-heading">
          <i class="fa fa-table"></i>Impressions Data in Table
        </div>
        <div class="widget-content padded clearfix">
          <div class="table-responsive">
            <table class="table analytic-table">
              <thead>
                <tr>
                  <th>S-N</th>
                  <th>Date & Time</th>
                  <th>Data URL</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>16<sup>th</sup> November, 2017 at 06:30am</td>
                  <td>https://www.google.com</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>16<sup>th</sup> November, 2017 at 06:30am</td>
                  <td>https://www.google.com</td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>16<sup>th</sup> November, 2017 at 06:30am</td>
                  <td>https://www.google.com</td>
                </tr>
                <tr>
                  <td>4</td>
                  <td>16<sup>th</sup> November, 2017 at 06:30am</td>
                  <td>https://www.google.com</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div><!-- impressions table -->
    <!-- clicks table -->
    <div class="col-md-6">
      <div class="widget-container fluid-height clearfix">
        <div class="heading custom-section-heading">
          <i class="fa fa-table"></i>Clicks Data in Table
        </div>
        <div class="widget-content padded clearfix">
          <div class="table-responsive">
            <table class="table analytic-table">
              <thead>
                <tr>
                  <th>S-N</th>
                  <th>Date & Time</th>
                  <th>Data URL</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>16<sup>th</sup> November, 2017 at 06:30am</td>
                  <td>https://www.google.com</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>16<sup>th</sup> November, 2017 at 06:30am</td>
                  <td>https://www.google.com</td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>16<sup>th</sup> November, 2017 at 06:30am</td>
                  <td>https://www.google.com</td>
                </tr>
                <tr>
                  <td>4</td>
                  <td>16<sup>th</sup> November, 2017 at 06:30am</td>
                  <td>https://www.google.com</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div><!-- clicks table ends here -->
  </div>
</div><!-- .analytics-wrapper ends -->

@php
  echo "<pre>";
  echo "<h2>Data-Analytics</h2>";
  print_r($analytics_data);
@endphp
@stop