/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(1);


/***/ }),
/* 1 */
/***/ (function(module, exports) {

$(document).ready(function () {

  App = {
    registerGlobalDomFunctions: function registerGlobalDomFunctions() {
      $('.beacon-check-label input[type="checkbox"]').on('click', function (evt) {
        var deleteButton = $('.beacon-del-btn');
        var deleteLabel = deleteButton.children('span.btn-text');
        var checkedBeacons = $('.beacon-check-label input[type="checkbox"]:checked').length;
        if (checkedBeacons == 0) {
          if (!deleteButton.hasClass('not-active')) {
            deleteButton.addClass('not-active');
          }
        } else if (checkedBeacons == 1) {
          deleteButton.removeClass('not-active');
          deleteLabel.html('Delete Beacon');
        } else {
          deleteButton.removeClass('not-active');
          deleteLabel.html('Delete Beacons');
        }
      });

      $('.analytic-type-select').on('change', function (evt) {
        var select_value = $(this).val();
        if (select_value == 'date_range') {
          $('#input-date-from, #input-date-until').fadeIn(300);
        } else {
          $('#input-date-from, #input-date-until').fadeOut(300);
        }
      });

      $('button.register-beacon-submit').on('click', function (e) {
        $('form#register-beacon-form').submit();
      });

      $('form#register-beacon-form').on('submit', function (e) {
        e.preventDefault();
        var url = "beacons/register-beacon"; // the script where you handle the form input.
        $.ajax({
          type: "POST",
          url: url,
          data: $(this).serialize(), // serializes the form's elements.
          success: function success(response) {
            console.log(response);
          },
          error: function error(response) {
            alert("There is problem with the server. Please try again later.");
          }
        });
      });
    } // registerGlobalDomFunctions()
  };
  App.registerGlobalDomFunctions();
});

/***/ })
/******/ ]);