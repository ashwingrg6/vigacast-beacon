@extends('layouts.master')
@section('main-content')
<div class="page-title">
  <li>YOU ARE HERE</li>
  <li><a href="javascript:void(0);">List/Edit Users</a></li>
</div>
<div class="all-users-wrapper main-content-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <!-- data/section heading -->
      <div class="widget-container fluid-height clearfix">
        <div class="heading table-heading beacon-section-heading">
          <div class="buttons-wrapper">
            <a class="label tbl-primary-btn red-bg" href="{{ url('/users/new') }}">
              <i class="ion-plus-circled ion-md-size"> </i>
              <span class="btn-text">&nbsp; New User</span>
            </a>
          </div>
        </div>
        <div class="widget-content padded clearfix body-padded">
          <!-- all users table -->
          <table class="table table-bordered table-striped general-tbl all-users-tbl" id="dataTable1">
            @if (Session::has('success_msg'))
              <div class="alert alert-success form-message">
                <button class="close" data-dismiss="alert" type="button">×</button>
                <i class="ion-checkmark-circled ion-md-size">&nbsp;</i>
                {{ Session::get('success_msg') }}
              </div>
            @endif
            @if (Session::has('app_error'))
              <div class="alert alert-danger form-message">
                <button class="close" data-dismiss="alert" type="button">×</button>
                <i class="ion-alert-circled ion-md-size">&nbsp;</i>
                {{ Session::get('app_error') }}
              </div>
            @endif
            <thead>
              <th>S-N</th>
              <th>Name</th>
              <th class="hidden-xs">Email</th>
              <th>User Type</th>
              <th class="hidden-xs">Date Added</th>
              <th></th>
            </thead>
            <tbody>
              <?php $sn = 1; ?>
              @foreach($users as $user)
                <tr>
                  <td>{{ $sn }}</td>
                  <td>{{ $user->name }}</td>
                  <td class="hidden-xs">{{ $user->email }}</td>
                  <td>{{ $user->type }}</td>
                  <td class="hidden-xs">
                    @if($user->created_at != '')
                      {{ $user->created_at }}
                    @else
                      {{ 'N/A' }}
                    @endif
                  </td>
                  <td class="actions">
                    <div class="action-buttons">
                      <a class="table-actions tooltip-trigger" href="{{ url('/users/edit').'/'.$user->id }}" data-placement="top" data-toggle="tooltip" data-original-title="Edit" href="#">
                        <i class="fa fa-pencil"></i>
                      </a>
                      @if($sn != 1)
                        <a class="table-actions tooltip-trigger" href="{{ url('/users/delete').'/'.$user->id }}" data-placement="right" data-toggle="tooltip" data-original-title="Delete" onclick="return confirm('Are you sure you want to delete this user?')">
                          <i class="fa fa-trash-o"></i>
                        </a>
                      @endif
                    </div>
                  </td>
                </tr>
              <?php $sn++; ?>
              @endforeach
            </tbody>
          </table><!-- all users table ends here -->
        </div><!-- .widget-container -->
      </div>
    </div>
  </div>
</div><!-- all users wrapper ends -->
@stop