<?php
/**
 * Function to detect environment as per the project's base path and load
 * relevant environment file.
 *
 * @var
 */
$env = $app->detectEnvironment(function () {
  switch (base_path()) {
    case '/Library/WebServer/Documents/vigacast-beacon':
      $env = 'local';
      break;
    // staging case
    default:
      $env = 'production';
      break;
  }
  $environmentPath = __DIR__ . '/../.' . $env . '.env';
  // echo $env;die;
  // $setEnv = trim(file_get_contents($environmentPath));
  if (file_exists($environmentPath)) {
    $dotenv = new Dotenv\Dotenv(__DIR__ . '/../', '.'.$env.'.env');
    $dotenv->overload();
  }
});

?>