@extends('layouts.master')
@section('main-content')
<div class="page-title">
  <li>YOU ARE HERE</li>
  <li><a href="javascript:void(0);">Add New User</a></li>
</div>
<div class="new-user-wrapper main-content-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <!-- data/section heading -->
      <div class="widget-container fluid-height clearfix">
        <div class="heading table-heading beacon-section-heading">
          <h3 class="page-section-heading">Register New User</h3>
        </div>
        <div class="widget-content padded clearfix body-padded">
          <!-- new user form starts -->
          <form class="general-form" method="post" novalidate="novalidate">
            {{ csrf_field() }}
            <fieldset>
              <!-- form messages -->
              <div class="row">
                <div class="col-md-8">
                  @if (Session::has('success_msg'))
                    <div class="alert alert-success form-message">
                      <button class="close" data-dismiss="alert" type="button">×</button>
                      <i class="ion-checkmark-circled ion-md-size">&nbsp;</i>
                      {{ Session::get('success_msg') }}
                    </div>
                  @endif
                  @if (!$errors->isEmpty())
                    <div class="alert alert-danger form-message">
                      <button class="close" data-dismiss="alert" type="button">×</button>
                      <i class="ion-alert-circled ion-md-size">&nbsp;</i>
                      Error! There are some errors, please correct them below.
                    </div>
                  @endif
                  @if (Session::has('app_error'))
                    <div class="alert alert-danger form-message">
                      <button class="close" data-dismiss="alert" type="button">×</button>
                      <i class="ion-alert-circled ion-md-size">&nbsp;</i>
                      {{ Session::get('app_error') }}
                    </div>
                  @endif
                </div>
              </div><!-- form messages section ends here -->
              <!-- new user, name input -->
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="input-name">Name *</label>
                    <input class="form-control" value="{{ old('name') }}" name="name" type="text" placeholder="Enter Name" id="input-name">
                    <label class="form-error">{{ $errors->first('name') }}</label>
                  </div>
                </div>
              </div><!-- new user, name input ends -->
              <!-- new user email-usertype inputs -->
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="input-email">Email *</label>
                    <input class="form-control" value="{{ old('email') }}" name="email" type="text" placeholder="Enter Email" id="input-email">
                    <label class="form-error">{{ $errors->first('email') }}</label>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="select-user-type">User Type *</label>
                    <select class="form-control" name="type" id="select-user-type">
                      <option value="">Select Below</option>
                      <option value="admin"
                      @if(old('type') == 'admin')
                        {{ 'selected' }}
                      @endif
                      >Admin</option>
                      <option value="viewer"
                      @if(old('type') == 'viewer')
                        {{ 'selected' }}
                      @endif>Viewer</option>
                    </select>
                    <label class="form-error">{{ $errors->first('type') }}</label>
                  </div>
                </div>
              </div><!-- email-usertype inputs end -->
              <!-- password and re-password inputs -->
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="input-psw">Password *</label>
                    <input class="form-control" name="password" type="password" placeholder="**********" id="input-psw">
                    <label class="form-error">{{ $errors->first('password') }}</label>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="input-re-psw">Re-Password *</label>
                    <input class="form-control" name="password_confirmation" type="password" placeholder="**********" id="input-re-psw">
                    <label class="form-error">{{ $errors->first('password_confirmation') }}</label>
                  </div>
                </div>
              </div><!-- password and re-password inputs end here -->
              <!-- form action buttons -->
              <div class="row form-actions">
                <div class="col-md-6">
                  <div class="form-group">
                    <button class="btn red-primary form-btn" type="submit">
                      <i class="ion-checkmark-circled ion-md-size">&nbsp;</i><span>Save</span>
                    </button>
                    <a href="{{ url('/users') }}">
                      <button class="btn gray-primary form-btn last-btn" type="button">
                      <i class="ion-close-circled ion-md-size">&nbsp;</i><span>Cancel</span>
                      </button>
                    </a>
                  </div>
                </div>
              </div><!-- form actions buttons end -->
            </fieldset>
          </form><!-- new user form ends here -->
        </div><!-- .widget-container -->
      </div>
    </div>
  </div>
</div><!-- new user wrapper ends -->
@stop