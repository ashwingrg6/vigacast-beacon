<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Log;
use DB;
use App\Http\Controllers\MyController;

class BeaconUrls extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'beacon_urls:change';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Change beacon attachment urls';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
    $this->my_controller = new MyController;
    $this->google_token_data = json_decode(file_get_contents(storage_path()."/json/google_token.json"), true);
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    // $this->my_controller->googleClient();
    $query_time = time()-240;
    $urls = DB::table('shortened_urls')->where('updated_at', '<=', $query_time)->get();
    if(count($urls) > 0) {
      foreach ($urls as $url) {
        // $new_code = $this->generateCode();
        $new_code = time();
        // $attachments = $this->addAttachment();
        // $attachments = $this->getAttachments();
        // if (!empty($attachments['attachments'])) {
        //   $attachment_data = $attachments['attachments'][0]['data'];
        //   $data = base64_decode($attachment_data);
        //   $json_data = json_decode($data, TRUE);
        //   $json_data['url'] = "https://vigacast.com/u/".$new_code;
        //   $new_data = json_encode($json_data);
        //   $this->deleteAttachment($attachments['attachments'][0]['attachmentName']);
        //   $this->addAttachment($new_data);
        //   DB::table('shortened_urls')
        //     ->where('id', $url->id)
        //     ->update(['code' => $new_code, 'updated_at' => time()]);
        // }
        DB::table('shortened_urls')
            ->where('id', $url->id)
            ->update(['code' => $new_code, 'updated_at' => time()]);
      }
    }
    Log::info('Completed Task Artisan Command Task: beacon_urls:change');
  }

  public function getAttachments() {
    // $beacon_name = "beacons/3!30303131323233333434313131323232";
    $beacon_name = "beacons/3!45324335364442354446443046354137";
    $client = new \GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer '.$this->google_token_data['access_token']], 'Accept' => 'application/json']);
    $res = $client->get("https://proximitybeacon.googleapis.com/v1beta1/$beacon_name/attachments?namespacedType=*/*");
    if ($res->getStatusCode() == 200) {
      $attachments = json_decode($res->getBody(), TRUE);
      return $attachments;
    }
    echo "error in get attachments";die;
  }

  public function deleteAttachment($attachment_name) {
    $client = new \GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer '.$this->google_token_data['access_token']], 'Accept' => 'application/json']);
    $res = $client->delete("https://proximitybeacon.googleapis.com/v1beta1/$attachment_name");
    if ($res->getStatusCode() == 200) {
      $attachments = json_decode($res->getBody(), TRUE);
      return $attachments;
    }
    echo "error in delete attachment";die;
  }

  public function addAttachment($data) {
    // $beacon_name = "beacons/3!30303131323233333434313131323232";
    $beacon_name = "beacons/3!45324335364442354446443046354137";
    $client = new \GuzzleHttp\Client(['headers' => ['Authorization' => 'Bearer '.$this->google_token_data['access_token']], 'Accept' => 'application/json']);
    $attachment_data = [
      "attachmentName" => "",
      "namespacedType" => "com.google.nearby/en",
      "data" => base64_encode($data),
      "creationTimeMs" => "2017-12-01T16:39:43.839Z"
    ];
    $res = $client->post("https://proximitybeacon.googleapis.com/v1beta1/$beacon_name/attachments",
      [\GuzzleHttp\RequestOptions::JSON => $attachment_data]
    );
    if ($res->getStatusCode() == 200) {
      $attachments = json_decode($res->getBody(), TRUE);
      return $attachments;
    }
    echo "error in add attachment";die;
  }

  function generateCode () {
    $code = $this->getRandomCode();
    while($this->verifyCode($code) != "ok") {
      $code = $this->getRandomCode();
    }
    return $code;
  }

  function getRandomCode () {
    $random_bytes = random_bytes(3);
    $code = bin2hex($random_bytes);
    return $code;
  }

  function verifyCode ($code) {
    $res = DB::table('shortened_urls')->where('code', '=', $code)->get();
    if(count($res) == 0) {
      return "ok";
    }
    return false;
  }

}
