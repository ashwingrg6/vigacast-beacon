<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachment_urls', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('beacon_id')->unsigned();
            $table->integer('short_url_id')->unsigned();
            $table->string('attachment_url_title');
            $table->string('attachment_url');
            $table->timestamps();
            $table->foreign('beacon_id')
                    ->references('id')
                    ->on('beacons')
                    ->onUpdate('cascade');
            $table->foreign('short_url_id')
                    ->references('id')
                    ->on('shortened_urls')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attachment_urls');
    }
}
