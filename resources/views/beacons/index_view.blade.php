@extends('layouts.master')
@section('main-content')
<div class="page-title">
  <li>YOU ARE HERE</li>
  <li><a href="javascript:void(0);">Beacons</a></li>
</div>
<div class="beacons-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <!-- data/section heading -->
      <div class="widget-container fluid-height clearfix">
        <div class="heading table-heading beacon-section-heading">
          <div class="buttons-wrapper">
            <a class="label tbl-primary-btn beacon-add-btn red-bg" href="#add-beacon-modal" data-toggle="modal">
              <i class="ion-plus-circled ion-md-size"> </i>
              <span class="btn-text">&nbsp;Add Beacon</span>
            </a>
            <a class="label tbl-primary-btn beacon-del-btn red-bg not-active" href="#">
              <i class="ion-trash-a ion-md-size">&nbsp;</i>
              <span class="btn-text">Delete Beacon</span>
            </a>
          </div>
          <div class="beacon-search-wrapper">
            <form action="#" class="form-horizontal" id="beacon-search-form">
              <div class="form-group">
                <div class="input-group">
                  <input class="form-control search-beacon-input text-dark-gray" type="text" placeholder="Search for IDs, names..."><span class="input-group-btn"><button class="btn btn-default btn-gray-bg" id="beacon-search-submit" type="submit">Search</button></span>
                </div>
              </div>
            </form>
          </div>
        </div><!-- .table-heading section ends -->
        <div class="widget-content padded clearfix table-wrapper">
          <div class="table-responsive">
            <table class="table beacons-table">
              <tbody>
                <tr class="data-content">
                  <td>
                    <label class="checkbox table-checkbox beacon-check-label"><input type="checkbox" class="beacon-check-input"><span class="beacon-select-checkbox"></span></label>
                  </td>
                  <td><i class="ion-ionic beacon-icon"></i></td>
                  <td class="beacon-info">
                    <span class="beacon-title">My Beacon Test1<br></span>
                    <span class="beacon-sub-data">
                      <span>UUID:&nbsp;</span><span>E2C56DB5-DFFB-48D2-B060-D0F5A71096E0<br></span>
                      <span>Advertise ID:&nbsp;</span><span>E2C56DB5D0D0F5A7</span>
                      <span>Location:&nbsp;</span><span>Bhaktapur<br></span>
                      <span>Major:&nbsp;</span><span>56</span>
                      <span>Minor:&nbsp;</span><span>14</span>
                      <span>Interval:&nbsp;</span><span>950ms</span>
                    </span>
                  </td>
                  <td>
                    <a class="label label-success tbl-primary-btn" href="#">
                      <i class="ion-ios-pulse-strong ion-md-size"> </i>
                      <span class="btn-text">&nbsp;Analytics</span>
                    </a>
                    <a class="label label-warning tbl-primary-btn btn-gray-bg no-margin-left" href="#">
                      <i class="ion-gear-b ion-md-size"></i>
                      <span class="btn-text">&nbsp;Edit Settings</span>
                    </a>
                  </td>
                </tr>
                <tr class="data-content">
                  <td>
                    <label class="checkbox table-checkbox beacon-check-label"><input type="checkbox"><span></span></label>
                  </td>
                  <td><i class="ion-ionic beacon-icon beacon-blue"></i></td>
                  <td class="beacon-info">
                    <span class="beacon-title">My Custom Beacon Test2<br></span>
                    <span class="beacon-sub-data">
                      <span>UUID:&nbsp;</span><span>E2C56DB5-DFFB-48D2-B060-D0F5A71096E0<br></span>
                      <span>Advertise ID:&nbsp;</span><span>E2C56DB5D0D0F5A7</span>
                      <span>Location:&nbsp;</span><span>Tinkune<br></span>
                      <span>Major:&nbsp;</span><span>56</span>
                      <span>Minor:&nbsp;</span><span>14</span>
                      <span>Interval:&nbsp;</span><span>950ms</span>
                    </span>
                  </td>
                  <td>
                    <a class="label label-success tbl-primary-btn" href="#">
                      <i class="ion-ios-pulse-strong ion-md-size"> </i>
                      <span class="btn-text">&nbsp;Analytics</span>
                    </a>
                    <a class="label label-warning tbl-primary-btn btn-gray-bg no-margin-left" href="#">
                      <i class="ion-gear-b ion-md-size"></i>
                      <span class="btn-text">&nbsp;Edit Settings</span>
                    </a>
                  </td>
                </tr>
                <tr class="data-content">
                  <td>
                    <label class="checkbox table-checkbox beacon-check-label"><input type="checkbox"><span></span></label>
                  </td>
                  <td><i class="ion-ionic beacon-icon beacon-green"></i></td>
                  <td class="beacon-info">
                    <span class="beacon-title">My Custom Beacon Test3<br></span>
                    <span class="beacon-sub-data">
                      <span>UUID:&nbsp;</span><span>E2C56DB5-DFFB-48D2-B060-D0F5A71096E0<br></span>
                      <span>Advertise ID:&nbsp;</span><span>E2C56DB5D0D0F5A7</span>
                      <span>Location:&nbsp;</span><span>Banepa<br></span>
                      <span>Major:&nbsp;</span><span>55</span>
                      <span>Minor:&nbsp;</span><span>19</span>
                      <span>Interval:&nbsp;</span><span>945ms</span>
                    </span>
                  </td>
                  <td>
                    <a class="label label-success tbl-primary-btn" href="#">
                      <i class="ion-ios-pulse-strong ion-md-size"> </i>
                      <span class="btn-text">&nbsp;Analytics</span>
                    </a>
                    <a class="label label-warning tbl-primary-btn btn-gray-bg no-margin-left" href="#">
                      <i class="ion-gear-b ion-md-size"></i>
                      <span class="btn-text">&nbsp;Edit Settings</span>
                    </a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div><!-- .table-responsive ends -->
        </div><!-- .table-wrapper ends -->
      </div><!-- .widget-container -->
      <!-- add beacon modal starts -->
      <div class="modal fade" id="add-beacon-modal" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
          <div class="modal-content" id="add-beacon-modal-content">
            @include('beacons.register_beacon_form')
          </div>
        </div>
      </div><!-- add beacon form, modal ends here -->
    </div>
  </div>
</div><!-- .beacons-wrapper ends -->
@stop